

!define NAMEPG 	"PostgreSQL"
!define SMALNAMEPG 	"pgsql"


!define VERMAJOR "8"
!define VERMINOR "3"

!define VERPG	"8.4.2"
!define sufix "eter"
!define REVPG	"1"

!define SSLPATH "..\..\OpenSSL-Win32"
!define LIBMINGWPATHFROM "\usr\i586-pc-mingw32\sys-root\mingw\bin\"
!define MSVCRPATHFROM "..\msvcr"
!define PGHEADPATHFROM "src"
!define PGADMINPATHFROM "..\pgadmin3"
!define LICENSEPATH "AdditionalFiles\lic"
!define TIMEZONEFROM "..\..\timezone\"



!define DEFAULTPORT	"5432"
!define DEFAULTENCODING	"UTF-8"
!define DEFAULTLOCALE	"Russia, Russia"


!define PRODUCTCODE 	"2804AADD-AD71-Ad7f-BE84-D10D36381DC9"


!define SERVICEDISPLAYNAME ${SMALNAMEPG}-${VERPG}-${sufix}${REVPG}